import * as THREE from 'three';
import * as dat from 'dat.gui';

import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

import { choseGeo } from './util/geo_util';
import { getMaterial } from './util/mat_util';
import { lightChoser } from './util/light_util';

let camera, scene, renderer, gui, controls;
let folderOne, folderTwo, folderThree, folderFour, folderFive, folderSix, folderSeven;
let lightFolderOne, lightFolderTwo;
let planeFolder;

let flag = false;

let planeProperty = {
  material: 'mesh',
  type: 'standard',
  showObject: 'plane',
  planeSize: 300,

  mapFile: 'none',

  bumpScale: 0.01,
  metalness: 0.1,
  roughness: 0.7,

  red: 120,
  blue: 120,
  green: 120
};

let sphereProperty = {
  material: 'mesh',
  type: 'basic',
  showObject: 'sphere',
  sphereRadius: 2,
  sphereWidth: 24,
  sphereHeight: 24,
}

let lightControllerOne = {
  type: 'spot',
  intensity: 2,
  red: 255,
  green: 255,
  blue: 180,
  x: 10,
  y: 100,
  z: 50
}

let lightControllerTwo = {
  type: 'spot',
  intensity: 2,
  red: 255,
  green: 255,
  blue: 180,
  x: 10,
  y: 100,
  z: -50
}

let effectControler = {
  showObject: 'box',

  material: 'mesh',
  type: 'standard',

  boxWidht: 10,
  boxHeight: 10,
  boxDepth: 10,

  sphereRadius: 10,
  sphereWidth: 10,
  sphereHeight: 10,

  coneRadius: 32,
  coneHeight: 10,
  coneRadiusSegment: 10,

  cylinderRadiusTop: 32,
  cylinderRadiusBot: 32,
  cylinderHeight: 10,
  cylinderRadiusSegment: 10,

  torusRadius: 32,
  torusTube: 10,
  torusRadiusSegment: 10,
  torusTubularSegment: 10,

  teaPotSize: 10,
  teaPotSegments: 10,
  teaPotBot: true,
  teaPotLid: true,
  teaPotBody: true,
  teaPotFitlid: false,
  teaPotBlinn: true,

  knotRadius: 9,
  knotTube: 2,
  knotTubeSeg: 50,
  knotRadiusSeg: 8,

  tubeRadius: 2,
  tubeRadiusSeg: 8,
  tubeClosed: false,

  paramSlices: 25,
  paramStacks: 25,

  red: 80,
  blue: 80,
  green: 80,

  mapFile: 'none',

  bumpScale: 0.01,
  metalness: 0.1,
  roughness: 0.7,

  posX: 0,
  posY: 0,
  posZ: 0,

  rotX: 0,
  rotY: 0,
  rotZ: 0,

  scaX: 0,
  scaY: 0,
  scaZ: 0,

  animation: false
};

const setupMaterial = () => {
  if (folderFour !== undefined) {
    gui.removeFolder(folderFour);
  }
  folderFour = gui.addFolder('material type');
  switch (effectControler.material) {
    case 'mesh':
      folderFour.add(effectControler, 'type', ['basic', 'lambert', 'phong', 'standard']).name('mesh material').onChange( renderObject );
      break;
    case 'line':
      folderFour.add(effectControler, 'type', ['basic', 'dashed']).name('line material').onChange( renderObject );
      break;
    default:
      break;
  }
  renderObject();
}


const setupProperty = () => {
  if (folderThree !== undefined) {
    gui.removeFolder(folderThree);
  }
  folderThree = gui.addFolder('property');
  switch (effectControler.showObject) {
    case 'box':
      folderThree.add(effectControler, 'boxWidht', 0, 100).name('box widht').onChange( renderObject );
      folderThree.add(effectControler, 'boxHeight', 0, 100).name('box height').onChange( renderObject );
      folderThree.add(effectControler, 'boxDepth', 0, 100).name('box depth').onChange( renderObject );
      break;
    case 'sphere':
      folderThree.add(effectControler, 'sphereRadius', 0, 100).name('sphere radius').onChange( renderObject );
      folderThree.add(effectControler, 'sphereWidth', 0, 100).name('sphere widht').onChange( renderObject );
      folderThree.add(effectControler, 'sphereHeight', 0, 100).name('sphere height').onChange( renderObject );
      break;
    case 'cone':
      folderThree.add(effectControler, 'coneRadius', 0, 100).name('cone radius').onChange( renderObject );
      folderThree.add(effectControler, 'coneHeight', 0, 100).name('cone height').onChange( renderObject );
      folderThree.add(effectControler, 'coneRadiusSegment', 0, 100).name('cone radius segment').onChange( renderObject );
      break;
    case 'cylinder':
      folderThree.add(effectControler, 'cylinderRadiusTop', 0, 100).name('cylinder radius top').onChange( renderObject );
      folderThree.add(effectControler, 'cylinderRadiusBot', 0, 100).name('cylinder radius bottom').onChange( renderObject );
      folderThree.add(effectControler, 'cylinderHeight', 0, 100).name('cylinder height').onChange( renderObject );
      folderThree.add(effectControler, 'cylinderRadiusSegment', 0, 100).name('cylinder radius segment').onChange( renderObject );
      break;
    case 'torus':
      folderThree.add(effectControler, 'torusRadius', 0, 100).name('torus radius').onChange( renderObject );
      folderThree.add(effectControler, 'torusTube', 0, 100).name('torus tube').onChange( renderObject );
      folderThree.add(effectControler, 'torusRadiusSegment', 0, 100).name('torus radius segment').onChange( renderObject );
      folderThree.add(effectControler, 'torusTubularSegment', 0, 100).name('torus tubular segment').onChange( renderObject );
      break;
    case 'teapot':
      folderThree.add(effectControler, 'teaPotSize', 0, 100).name('teapot size').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotSegments', 0, 100).name('teapot segment').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotBot').name('bottom').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotLid').name('lid').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotBody').name('body').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotFitlid').name('fit').onChange( renderObject );
      folderThree.add(effectControler, 'teaPotBlinn').name('blinn').onChange( renderObject );
      break;
    case 'knot':
      folderThree.add(effectControler, 'knotRadius', 0, 100).name('knot radius').onChange( renderObject );
      folderThree.add(effectControler, 'knotTube', 0, 100).name('knot tube').onChange( renderObject );
      folderThree.add(effectControler, 'knotTubeSeg', 0, 100).name('knot tubular segment').onChange( renderObject );
      folderThree.add(effectControler, 'knotRadiusSeg', 0, 100).name('knot radius segment').onChange( renderObject );
      break;
    case 'tube':
      folderThree.add(effectControler, 'tubeRadius', 0, 50).name('tube radius').onChange( renderObject );
      folderThree.add(effectControler, 'tubeRadiusSeg', 0, 39).name('tube radius segment').onChange( renderObject );
      folderThree.add(effectControler, 'tubeClosed').name('tube close').onChange( renderObject );
      break;
    case 'parametic':
      break;
    default:
      break;
  }
  renderObject();
}


const setupGUI = () => {
  gui = new dat.GUI({name: 'effect controler'});

  folderOne = gui.addFolder('object');
  folderOne.add(effectControler, 'showObject', ['box', 'sphere', 'cone', 'cylinder', 'torus', 'teapot', 'knot', 'tube', 'parametic']).name('object').onChange( setupProperty );
  
  folderTwo = gui.addFolder('material');
  folderTwo.add(effectControler, 'material', ['mesh', 'point', 'line']).name('draw material with').onChange( setupMaterial );

  folderFive = gui.addFolder('object color');
  folderFive.add(effectControler, 'red', 0, 255).name('red').onChange( renderObject );
  folderFive.add(effectControler, 'blue', 0, 255).name('blue').onChange( renderObject );
  folderFive.add(effectControler, 'green', 0, 255).name('green').onChange( renderObject );

  folderSix = gui.addFolder('object texture');
  folderSix.add(effectControler, 'mapFile', ['none', 'concrete.jpeg', 'road.jpeg', 'weeb.jpeg']).name('image texture').onChange( renderObject );
  folderSix.add(effectControler, 'bumpScale', 0, 1).name('bump').onChange( renderObject );
  folderSix.add(effectControler, 'metalness', 0, 1).name('metal').onChange( renderObject );
  folderSix.add(effectControler, 'roughness', 0, 1).name('rough').onChange( renderObject );

  folderSix = gui.addFolder('object transform');
  folderSix.add(effectControler, 'posX', -20, 20).name('position x').onChange( renderObject );
  folderSix.add(effectControler, 'posY', -20, 20).name('position y').onChange( renderObject );
  folderSix.add(effectControler, 'posZ', -20, 20).name('position z').onChange( renderObject );
  folderSix.add(effectControler, 'rotX', -20, 20).name('rotate x').onChange( renderObject );
  folderSix.add(effectControler, 'rotY', -20, 20).name('rotate y').onChange( renderObject );
  folderSix.add(effectControler, 'rotZ', -20, 20).name('rotate z').onChange( renderObject );
  folderSix.add(effectControler, 'scaX', -20, 20).name('scale x').onChange( renderObject );
  folderSix.add(effectControler, 'scaY', -20, 20).name('scale y').onChange( renderObject );
  folderSix.add(effectControler, 'scaZ', -20, 20).name('scale z').onChange( renderObject );

  folderSeven = gui.addFolder('object animation');
  folderSeven.add(effectControler, 'animation').name('animation');

  setupMaterial();
  setupProperty();
}


const setupLightGUI = () => {
  lightFolderOne = gui.addFolder('light 1');
  lightFolderOne.add(lightControllerOne, 'type', ['point', 'spot', 'directional', 'ambient']).name('light type').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'intensity', 0, 5).name('intensity').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'red', 0, 255).name('red').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'blue', 0, 255).name('blue').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'green', 0, 255).name('green').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'x', -100, 100).name('x').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'y', -100, 100).name('y').onChange( renderLight );
  lightFolderOne.add(lightControllerOne, 'z', -100, 100).name('z').onChange( renderLight );

  lightFolderTwo = gui.addFolder('light 2');
  lightFolderTwo.add(lightControllerTwo, 'type', ['point', 'spot', 'directional', 'ambient']).name('light type').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'intensity', 0, 5).name('intensity').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'red', 0, 255).name('red').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'blue', 0, 255).name('blue').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'green', 0, 255).name('green').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'x', -100, 100).name('x').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'y', -100, 100).name('y').onChange( renderLight );
  lightFolderTwo.add(lightControllerTwo, 'z', -100, 100).name('z').onChange( renderLight );
}

const setupPlaneGUI = () => {
  planeFolder = gui.addFolder('plane texture controller');
  planeFolder.add(planeProperty, 'mapFile', ['none', 'concrete.jpeg', 'road.jpeg', 'weeb.jpeg']).name('image texture').onChange( renderPlane );
  planeFolder.add(planeProperty, 'red', 0, 255).name('red').onChange( renderPlane );
  planeFolder.add(planeProperty, 'blue', 0, 255).name('blue').onChange( renderPlane );
  planeFolder.add(planeProperty, 'green', 0, 255).name('green').onChange( renderPlane );
  planeFolder.add(planeProperty, 'bumpScale', 0, 1).name('bump').onChange( renderPlane );
  planeFolder.add(planeProperty, 'metalness', 0, 1).name('metal').onChange( renderPlane );
  planeFolder.add(planeProperty, 'roughness', 0, 1).name('rough').onChange( renderPlane );
}


const renderObject = () => {
  if (scene.getObjectByName('shown_object') !== undefined) {
    scene.remove(scene.getObjectByName('shown_object'));
  }
  
  //object
  let showObjectMat = getMaterial(effectControler, {
    color: `rgb(${parseInt(effectControler.red)}, ${parseInt(effectControler.green)}, ${parseInt(effectControler.blue)})`
  });
  let objectGeo = choseGeo(effectControler, showObjectMat);
  objectGeo.name = 'shown_object'

  if (effectControler.showObject === 'sphere') {
    objectGeo.position.y = objectGeo.geometry.parameters.radius;
  }

  if (
    effectControler.showObject === 'box' ||
    effectControler.showObject === 'cone' ||
    effectControler.showObject === 'cylinder'
  ) {
    objectGeo.position.y = objectGeo.geometry.parameters.height/2;
  }

  if (effectControler.showObject === 'torus') {
    objectGeo.position.y = objectGeo.geometry.parameters.radius + objectGeo.geometry.parameters.tube;
  }

  if (effectControler.showObject === 'teapot') {
    objectGeo.position.y = 10;
  }

  if (effectControler.showObject === 'knot') {
    objectGeo.position.y = objectGeo.geometry.parameters.radius + 7;
  }

  if (effectControler.showObject === 'tube') {
    objectGeo.position.y = objectGeo.geometry.parameters.radius*2 + 7;
  }

  if (effectControler.showObject === 'parametic') {
    objectGeo.position.y = 7;
  }

  objectGeo.position.x += effectControler.posX;
  objectGeo.position.y += effectControler.posY;
  objectGeo.position.z += effectControler.posZ;

  objectGeo.rotation.x += effectControler.rotX;
  objectGeo.rotation.y += effectControler.rotY;
  objectGeo.rotation.z += effectControler.rotZ;

  objectGeo.scale.x += effectControler.scaX;
  objectGeo.scale.y += effectControler.scaY;
  objectGeo.scale.z += effectControler.scaZ;

  scene.add(objectGeo);
}

const renderLight = () => {
  if (scene.getObjectByName('light-1') !== undefined) {
    scene.remove(scene.getObjectByName('light-1'));
  }
  if (scene.getObjectByName('light-2') !== undefined) {
    scene.remove(scene.getObjectByName('light-2'));
  }

  //light 1
  let lightSphereOne = getMaterial(sphereProperty, {
    color: 'rgb(255, 255, 255)'
  })
  let lightSphereOneGeo = choseGeo(sphereProperty, lightSphereOne);
  let lightOne = lightChoser(lightControllerOne);
  lightOne.name = 'light-1';

  //light 2
  let lightSphereTwo = getMaterial(sphereProperty, {
    color: 'rgb(255, 255, 255)'
  })
  let lightSphereTwoGeo = choseGeo(sphereProperty, lightSphereTwo);
  let lightTwo = lightChoser(lightControllerTwo);
  lightTwo.name = 'light-2';

  lightOne.add(lightSphereOneGeo);
  lightTwo.add(lightSphereTwoGeo);
  scene.add(lightOne);
  scene.add(lightTwo);
}


const renderPlane = () => {
  if (scene.getObjectByName('plane-1') !== undefined) {
    scene.remove(scene.getObjectByName('plane-1'));
  }

  let planeMat = getMaterial(planeProperty, {
    side: THREE.DoubleSide,
    color: `rgb(${parseInt(planeProperty.red)}, ${parseInt(planeProperty.green)}, ${parseInt(planeProperty.blue)})`
  });
  let plane = choseGeo(planeProperty, planeMat);
  plane.name = 'plane-1'
  plane.rotation.x = Math.PI/2;

  scene.add(plane);
}


const main = () => {
  let axesHelper = new THREE.AxesHelper( 5 );

  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth/window.innerHeight,
    1,
    1000
  );

  renderObject();
  renderLight();
  renderPlane();
  setupGUI();
  setupLightGUI();
  setupPlaneGUI();

  scene.add(camera);
  
  scene.add(axesHelper);

  camera.position.x = 100;
  camera.position.y = 100;
  camera.position.z = 100;

  let vectorA = new THREE.Vector3(0, 0, 0);
  camera.lookAt(vectorA);

  renderer = new THREE.WebGLRenderer();
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  renderer.setClearColor('rgb(120, 120, 120)');
  document.getElementById('app').appendChild(renderer.domElement);

  controls = new OrbitControls(camera, renderer.domElement);

  update(renderer, scene, camera, controls);
}

const update = (renderer, scene, camera, controls) => {
  renderer.render(
    scene,
    camera
  );

  controls.update()

  if (effectControler.animation === true) {
    let animationOb = scene.getObjectByName('shown_object')
    if (animationOb.position.y < 70 && flag === false) {
      animationOb.position.y += 0.5;
    } else if (animationOb.position.y >= 70) {
      flag = true;
    }
  
    if (animationOb.position.y > 30 && flag === true) {
      animationOb.position.y -= 0.5;
    } else if (animationOb.position.y <= 30) {
      flag = false;
    }
  
    animationOb.rotation.x += 0.1;
    animationOb.rotation.y += 0.01;
    animationOb.rotation.z += 0.05;
  }

  requestAnimationFrame(function() {
    update(renderer, scene, camera, controls);
  });
}

main()