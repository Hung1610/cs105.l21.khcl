# CS105.L22.KHCL project
## Prerequisites

* node 15.11.0
* npm 7.6.0

## Develop setup

1. install lib:

```
npm install
```
or

```
npm i
```

2. run dev server:

```
npm run dev
```

## Resource

1. gitlab:

```
https://gitlab.com/Hung1610/cs105.l21.khcl
```

2. drive:

```
https://drive.google.com/drive/folders/1WzH6eyK6Bq5lUmtDKlG8O0FMajnAaZGg?usp=sharing
```

3. demo:

```
https://drive.google.com/drive/folders/1WzH6eyK6Bq5lUmtDKlG8O0FMajnAaZGg?usp=sharing
```

or

```
https://gitlab.com/Hung1610/cs105.l21.khcl/-/tree/master/b%C3%A9o_c%C3%A9o
```