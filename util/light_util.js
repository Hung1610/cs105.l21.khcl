import * as THREE from 'three';

const lightChoser = (lightController) => {
    let lightObject = null;
    let color = `rgb(${parseInt(lightController.red)}, ${parseInt(lightController.green)}, ${parseInt(lightController.blue)})`;
    
    switch (lightController.type) {
        case 'point':
            lightObject = getPointLight(color, lightController.intensity);
            lightObject.position.set(lightController.x, lightController.y, lightController.z);
            break;
        case 'spot':
            lightObject = getSpotLight(color, lightController.intensity);
            lightObject.position.set(lightController.x, lightController.y, lightController.z);
            break;
        case 'directional':
            lightObject = getDirectionalLight(color, lightController.intensity);
            lightObject.position.set(lightController.x, lightController.y, lightController.z);
            break;
        case 'ambient':
            lightObject = getAmbientLight(color, lightController.intensity);
            break;
        default:
            lightObject = getPointLight(color, lightController.intensity);
            lightObject.position.set(lightController.x, lightController.y, lightController.z);
            break;
    }

    return lightObject;
}

const getPointLight = (color, intensity) => {
  let light = new THREE.PointLight(color, intensity);
  light.castShadow = true;

  return light;
}

const getSpotLight = (color, intensity) => {
  let light = new THREE.SpotLight(color, intensity);
  light.castShadow = true;

  light.shadow.bias = 0.001;
  light.shadow.mapSize.width = 2048;
  light.shadow.mapSize.height = 2048;

  return light;
}

const getDirectionalLight = (color, intensity) => {
  let light = new THREE.DirectionalLight(color, intensity);
  light.castShadow = true;

  light.shadow.camera.left = -10
  light.shadow.camera.bottom = -10
  light.shadow.camera.right = 10
  light.shadow.camera.top = 10

  return light;
}

const getAmbientLight = (color, intensity) => {
  let light = new THREE.AmbientLight(color, intensity);
  light.castShadow = true;

  return light;
}

export {
  lightChoser
}