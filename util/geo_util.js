import * as THREE from 'three';
import { TeapotGeometry } from "three/examples/jsm/geometries/TeapotGeometry";
import { ParametricGeometries } from "three/examples/jsm/geometries/ParametricGeometries";

class CustomSinCurve extends THREE.Curve {
    constructor(scale = 1) {
        super();
        this.scale = scale;
    }

    getPoint(t, optionalTarget = new THREE.Vector3()) {
        const tx = t * 3 - 1.5;
        const ty = Math.sin(2 * Math.PI * t);
        const tz = 0;
        return optionalTarget.set(tx, ty, tz).multiplyScalar(this.scale);
    }
}
const path = new CustomSinCurve(10);

const choseGeo = (effectControler, material) => {
    let shape = null;
    let geometry = null;

    switch (effectControler.showObject) {
        case 'plane':
            geometry = getPlaneGeo(effectControler.planeSize);
            break;
        case 'box':
            geometry = getBoxGeo(effectControler.boxWidht, effectControler.boxHeight, effectControler.boxDepth);
            break;
        case 'sphere':
            geometry = getSphereGeo(effectControler.sphereRadius, effectControler.sphereWidth, effectControler.sphereHeight);
            break;
        case 'cone':
            geometry = getConeGeo(effectControler.coneRadius, effectControler.coneHeight, effectControler.coneRadiusSegment);
            break;
        case 'cylinder':
            geometry = getCylinderGeo(effectControler.cylinderRadiusTop, effectControler.cylinderRadiusBot, effectControler.cylinderHeight, effectControler.cylinderRadiusSegment);
            break;
        case 'torus':
            geometry = getTorusGeo(effectControler.torusRadius, effectControler.torusTube, effectControler.torusRadiusSegment, effectControler.torusTubularSegment);
            break;
        case 'teapot':
            geometry = getTeaPotGeo(effectControler.teaPotSize, effectControler.teaPotSegments, effectControler.teaPotBot, effectControler.teaPotLid, effectControler.teaPotBody, effectControler.teaPotFitlid, effectControler.teaPotBlinn);
            break;
        case 'knot':
            geometry = getKnotGeo(effectControler.knotRadius, effectControler.knotTube, effectControler.knotTubeSeg, effectControler.knotRadiusSeg);
            break;
        case 'tube':
            geometry = getTubeGeo(effectControler.tubeRadius, effectControler.tubeRadiusSeg, effectControler.tubeClosed);
            break;
        case 'parametic':
            geometry = getParameticGeo();
            break;
        default:
            geometry = getBoxGeo(effectControler.boxWidht, effectControler.boxHeight, effectControler.boxDepth);
            break;
    }

    if (effectControler.material === 'line') {
        shape = new THREE.Line(
            geometry,
            material
        )
    }

    if (effectControler.material === 'point') {
        shape = new THREE.Points(
            geometry,
            material
        )
    }

    if (effectControler.material === 'mesh') {
        shape = new THREE.Mesh(
            geometry,
            material
        )
    }

    if (effectControler.showObject === 'plane') {
        shape.receiveShadow = true;
    } else {
        shape.castShadow = true;
    }

    return shape;
}

const getPlaneGeo = ( size ) => {
    const geometry = new THREE.PlaneGeometry(size, size);
  
    return geometry;
}

const getBoxGeo = ( w, h, d ) => {
    const geometry = new THREE.BoxGeometry(w, h, d);
  
    return geometry;
  }

const getSphereGeo = ( r, w, h ) => {
    const geometry = new THREE.SphereGeometry(r, w, h);

    return geometry;
}

const getConeGeo = ( r, h, rs ) => {
    const geometry = new THREE.ConeGeometry(r, h, rs);

    return geometry;
}

const getCylinderGeo = ( rt, rb, h, rs ) => {
    const geometry = new THREE.CylinderGeometry(rt, rb, h, rs);

    return geometry;
}

const getTorusGeo = ( r, t, rs, ts ) => {
    const geometry = new THREE.TorusGeometry(r, t, rs, ts);

    return geometry;
}

const getTeaPotGeo = ( size, segments, bot, lid, body, fitlid, blinn ) => {
    const geometry = new TeapotGeometry(
        size, segments,
        bot, lid, body, fitlid, blinn
    );

    return geometry;
}

const getKnotGeo = ( r, t, ts, rs ) => {
    const geometry = new THREE.TorusKnotBufferGeometry(r, t, ts, rs);

    return geometry;
}

const getTubeGeo = ( r, rs, closed ) => {
    const geometry = new THREE.TubeGeometry(path, 20, r, rs, closed);

    return geometry;
}

const getParameticGeo = () => {
    const geometry = new THREE.ParametricGeometry(ParametricGeometries.klein, 25, 25);

    return geometry;
}

export {
    choseGeo
};