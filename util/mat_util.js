import * as THREE from 'three';

function getMaterial(effectControler, extraData) {
    let loader = new THREE.TextureLoader();
    let selectiveMaterial = null;
    let materialOption = null;

    if (effectControler.material === 'mesh') {
        materialOption = {
            color: extraData.color === undefined ? 'rgb(255, 255, 255)' : extraData.color,
            side: extraData.side === undefined ? 'rgb(255, 255, 255)' : extraData.side
        };
        switch (effectControler.type) {
            case 'basic':
                selectiveMaterial = new THREE.MeshBasicMaterial(materialOption);
                break;
            case 'lambert':
                selectiveMaterial = new THREE.MeshLambertMaterial(materialOption);
                break;
            case 'phong':
                selectiveMaterial = new THREE.MeshPhongMaterial(materialOption);
                break;
            case 'standard':
                selectiveMaterial = new THREE.MeshStandardMaterial(materialOption);
                break;
            default:
                selectiveMaterial = new THREE.MeshBasicMaterial(materialOption);
                break;
        }
        if (effectControler.mapFile !== 'none') {
            selectiveMaterial.map = loader.load(`../img/${effectControler.mapFile}`);
            selectiveMaterial.bumpMap = loader.load(`../img/${effectControler.mapFile}`);
            selectiveMaterial.roughnessMap = loader.load(`../img/${effectControler.mapFile}`);
            selectiveMaterial.bumpScale = effectControler.bumpScale;
            selectiveMaterial.metalness = effectControler.metalness;
            selectiveMaterial.roughness = effectControler.roughness;
        }
    }

    if (effectControler.material === 'point') {
        materialOption = {
            color: extraData.color === undefined ? 'rgb(255, 255, 255)' : extraData.color
        };
        selectiveMaterial = new THREE.PointsMaterial(materialOption);
        selectiveMaterial.morphTargets = true;
    }

    if (effectControler.material === 'line') {
        materialOption = {
            color: extraData.color === undefined ? 'rgb(255, 255, 255)' : extraData.color,
            lineWidth: extraData.lineWidth === undefined ? 5 : extraData.lineWidth
        };
        switch (effectControler.type) {
            case 'basic':
                selectiveMaterial = new THREE.LineBasicMaterial(materialOption);
                break;
            case 'dashed':
                selectiveMaterial = new THREE.LineDashedMaterial(materialOption);
                break;
            default:
                selectiveMaterial = new THREE.LineBasicMaterial(materialOption);
                break;
        }
    }

    return selectiveMaterial;
}

export {
    getMaterial
}